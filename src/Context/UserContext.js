import { createContext, useState } from "react";

export const UserContext = createContext({ name: '', auth: false });

export const UserProvider = ({children}) => {

    const [user, setUser] = useState({name: "", email: "", auth: false})

    const login = (name, email, auth) => {
        setUser(name,email, auth)
    }

    const logout = () => {
        setUser({name:"", auth: false})
    }
    return(
        <UserContext.Provider value={{user, login, logout}}>
            {children}
        </UserContext.Provider>
    )
}