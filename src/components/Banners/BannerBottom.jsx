import { Box, Container, Icon, Typography } from '@mui/material';
import React from 'react'
import CallIcon from '@mui/icons-material/Call';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';

const BannerBottom = () => {
    const boxIconStyle = {
        display: "flex",
        color: "#FFFFFF",
        width: "45px",
        height: "45px",
        backgroundColor: "#FABC1D",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: "50%"
    };
    return (
        <Box sx={{
            display: "flex", justifyContent: "space-around", alignItems: "top",
            backgroundColor: "#5B4947",
            padding: "24px 64px",
            marginTop: "96px",
            columnGap: "32px",
            flexDirection: { md: 'row', sm: 'column', xs: 'column' }
        }} >
            <Container>
                <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                    About Us
                </Typography>
                <Typography textAlign="justify" mt={2} color="#FFFFFF" fontSize="14px" lineHeight="24px">
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </Typography>
            </Container>

            <Box sx={{
                padding: "0px 10px"
            }}>
                <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                    Product
                </Typography>
                <Box sx={{
                    display: "flex",
                    columnGap: "16px",
                    padding: "none",
                    justifyContent: "Start"
                }}>
                    <Box sx={{
                        padding: "0px 8px"
                    }}>
                        <Typography fontSize="14px" lineHeight="18px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }} >
                            &bull; Electric
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }} >
                            &bull; LCGC
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }} >
                            &bull; Offroad
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }} >
                            &bull; SUV
                        </Typography>
                    </Box>

                    <Box sx={{
                        padding: "0px 8px",
                        marginLeft: "20px"
                    }}>
                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }} >
                            &bull; Hatchback
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }}>
                            &bull; MPV
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }}>
                            &bull; Sedan
                        </Typography>

                        <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF" sx={{ width: 'max-content' }}>
                            &bull; Truck
                        </Typography>
                    </Box>
                </Box>
            </Box>
            <Container sx={{
                display: "flex",
                flexDirection: "column"
            }}>
                <Container>
                    <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                        Address
                    </Typography>
                    <Typography fontSize="14px" lineHeight="24px" textAlign="justify" mt={2} color="#FFFFFF">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                    </Typography>
                </Container>
                <Container sx={{
                    marginTop: "16px"
                }}>
                    <Typography variant="p" textAlign="Left" color="#FABC1D" fontSize="18px" fontWeight="500">
                        Contact Us
                    </Typography>
                    <Box sx={{
                        display: "flex",
                        columnGap: "16px",
                        padding: "none",
                        marginTop: "16px",
                        marginLeft: "8px"
                    }}>
                        <Box style={boxIconStyle}>
                            <CallIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <InstagramIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <YouTubeIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <TelegramIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                        <Box style={boxIconStyle}>
                            <EmailIcon style={{ color: "#5B4947", fontSize: "26px" }} />
                        </Box>
                    </Box>
                </Container>

            </Container>
        </Box>
    );
}

export default BannerBottom