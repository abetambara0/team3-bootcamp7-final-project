import { Box, Container, Typography } from "@mui/material";
import React from "react";
import banner from '../../assets/banner-bottom.png'

const BannerMid = () => {
    return (
        <Box sx={{
            display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", backgroundImage: `url(${banner})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            marginTop: "96px"
        }} >
            <Container sx={{ padding: '10px' }}>
                <Typography variant="h4" textAlign="Left" color="#FFFFFF" >
                    Gets your best benefit
                </Typography>
                <Typography fontSize="19px" textAlign="justify" mt={2} color="#FFFFFF">
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                    sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam.
                </Typography>
            </Container>
        </Box>
    );
};

export default BannerMid;
