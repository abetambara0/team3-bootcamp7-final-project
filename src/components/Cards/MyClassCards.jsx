import React from "react";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';




const MyClassCards = ({ img, category, name, price }) => {
    return (
        <Box sx={{ minWidth: 275, maxWidth: 350, display: "flex" }}>
            <Card variant="outlined" sx={{ minHeight: 210, minWidth: 350 }}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="140"
                        image={img}
                        alt="pictures"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="p" component="div" color="#828282">
                            {category}
                        </Typography>
                        <Typography gutterBottom variant="h6" component="div" color="#5B4947" >
                            {name}
                        </Typography>
                        <Typography variant="h5" color="#FABC1D" fontWeight="bold">
                            {price}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Box>
    );
};

export default MyClassCards;
