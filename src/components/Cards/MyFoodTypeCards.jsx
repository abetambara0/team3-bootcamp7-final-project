import React from "react";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';




const MyFoodTypeCards = ({ img, category}) => {
    return (
        <Box sx={{ minWidth: 125, maxWidth: 250, display: "flex" }}>
            <Card variant="outlined" sx={{ minHeight: 143, minWidth: 250 }}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        width="250"
                        height="143"
                        image={img}
                        alt="pictures"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div" textAlign="center">
                            {category}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Box>
    );
};

export default MyFoodTypeCards;
