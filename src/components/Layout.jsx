import React from "react";
import BannerBottom from "./Banners/BannerBottom";
import Navbar from "./Navbar";

const Layout = ({ children }) => {
    return (
        <>
            <Navbar />
            {children}
            <BannerBottom />
        </>
    );
};

export default Layout;
