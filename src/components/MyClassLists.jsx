import React from "react";
import {
    Box,
    Card,
    CardActionArea,
    CardContent,
    CardMedia,
    Typography,
} from "@mui/material";
import img from '../assets/tom-yum-thailand.png'


const MyClassLists = () => {
    return (
        <Card>
            <CardActionArea>
                <Box
                    sx={{
                        display: "flex",
                        flexFlow: "row wrap",
                        justifyContent: "flex-start",
                        margin: "40px 71px 24px",
                    }}
                >
                    <CardMedia
                        component="img"
                        sx={{ width: 200 }}
                        image={img}
                    />
                    <CardContent>
                        <Box
                            sx={{
                                display: "flex",
                                flexDirection: "column",
                                gap: "4px",
                            }}
                        >
                            <Typography sx={{ fontSize: 14 }} color="text.secondary">
                                Asian
                            </Typography>
                            <Typography
                                sx={{ fontSize: 16, fontWeight: "600" }}
                                gutterBottom
                                component="div"
                            >
                                Tom Yum Thailand
                            </Typography>
                        </Box>
                        <Typography
                            sx={{ fontSize: 20, fontWeight: "500" }}
                            color="#FABC1D"
                            gutterBottom
                            component="div"
                        >
                            Schedule : Wednesday, 27 July 2022
                        </Typography>
                    </CardContent>
                </Box>
            </CardActionArea>
        </Card>

    );
};

export default MyClassLists;
