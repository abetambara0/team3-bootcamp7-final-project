import MenuIcon from '@mui/icons-material/Menu';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import React, { useContext } from "react";
import logo from '../assets/logo.png';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../Context/UserContext'

const pages = ['Products', 'Pricing', 'Blog'];
const settings = ['My Cart', "My Class", 'Invoice', 'Profile'];

const Navbar = () => {

    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);
    const navigate = useNavigate()
    const { user, logout } = useContext(UserContext)
    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };


    return (
        <AppBar position="static" sx={{ backgroundColor: "#ffff" }}>
            {user?.name !== "" ?
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <Box sx={{ display: { xs: 'none', md: 'flex' }, mr: 1, cursor: 'pointer' }}
                            onClick={() => navigate('/')}
                        >
                            <img src={logo} alt="logo" />
                        </Box>
                        <Typography
                            variant="h6"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                mr: 2,
                                display: { xs: 'none', md: 'flex' },
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                        </Typography>

                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="#5B4947"
                            >
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: { xs: 'block', md: 'none' },
                                }}
                            >
                                {settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseNavMenu}>
                                        <Typography textAlign="center">{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                        <Box sx={{ display: { xs: 'flex', md: 'none' }, mr: 1, cursor: 'pointer' }} onClick={() => navigate('/')} >
                            <img src={logo} />
                        </Box>
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href=""
                            sx={{
                                mr: 2,
                                display: { xs: 'flex', md: 'none' },
                                flexGrow: 1,
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                        </Typography>
                        <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: "flex-end", alignItems: 'center', gap: "10px", }}>
                            <Button
                                onClick={handleCloseNavMenu}
                                sx={{ color: '#5B4947', display: 'block' }}
                            >
                                <ShoppingCartIcon fontSize="medium" />
                            </Button>
                            <Button
                                onClick={() => navigate('/myclass')}
                                sx={{ color: '#5B4947', display: 'block' }}
                            >
                                <Typography sx={{ textTransform: 'capitalize' }} fontSize="medium">
                                    My Class
                                </Typography>
                            </Button>
                            <Button
                                onClick={() => navigate('/invoice')}
                                sx={{ color: '#5B4947', display: 'block' }}
                            >
                                <Typography sx={{ textTransform: 'capitalize' }} fontSize="medium">
                                    Invoice
                                </Typography>
                            </Button>
                            <Divider orientation="vertical" flexItem sx={{ margin: "0 15px" }} />
                            <Button
                                onClick={handleCloseNavMenu}
                                sx={{ color: '#FABC1D', display: 'block' }}
                            >
                                <PersonIcon fontSize="medium" />
                            </Button>
                            <Button
                                onClick={() => logout()}
                                sx={{ color: '#5B4947', display: 'block' }}
                            >
                                <LogoutIcon fontSize="medium" />
                            </Button>
                        </Box>

                        <Box sx={{ flexGrow: 0 }}>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                        <Typography textAlign="center">{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
                :
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <Box sx={{ display: { xs: 'none', md: 'flex' }, mr: 1, cursor: 'pointer' }} onClick={() => navigate('/')}>
                            <img src={logo} />
                        </Box>

                        <Box sx={{ display: { xs: 'flex', md: 'none' }, mr: 1, cursor: 'pointer' }} onClick={() => navigate('/')}>
                            <img src={logo} />
                        </Box>
                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'flex' }, justifyContent: "flex-end", alignItems: 'center', gap: "10px", }}>
                            <Button
                                onClick={() => navigate('/register')}
                                sx={{ margin: "0 10px", color: '#5B4947', borderRadius: "8px", display: 'block', border: "1px solid #5B4947", textDecoration: "none" }}
                            >
                                Register
                            </Button>
                            <Button
                                onClick={() => navigate('/login')}
                                sx={{ margin: "0 10px", color: '#5B4947', borderRadius: "8px", display: 'block', backgroundColor: "#FABC1D" }}
                            >
                                Login
                            </Button>
                        </Box>

                        <Box sx={{ flexGrow: 0 }}>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                        <Typography textAlign="center">{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>}
        </AppBar>
    );
};

export default Navbar;
