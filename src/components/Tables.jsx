import React from "react";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button, Typography } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: '#5B4947',
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));



const Tables = ({ rows, heads, page }) => {
  const navigate = useNavigate()
  const location = useLocation()
  console.log(location.state)
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            {heads.map((head) => {
              return (
                <StyledTableCell align="center">{head}</StyledTableCell>
              )
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.No}>
              <StyledTableCell component="th" scope="row">
                {row.No}
              </StyledTableCell>
              <StyledTableCell align="center">{row.NoInvoice ? row.NoInvoice : row.CourseName}</StyledTableCell>
              <StyledTableCell align="center">{row.Date ? row.date : row.Type}</StyledTableCell>
              <StyledTableCell align="center">{row.TotalCourse ? row.TotalCourse : row.Schedule}</StyledTableCell>
              <StyledTableCell align="center">{row.TotalPrice ? row.TotalPrice : row.Price}</StyledTableCell>
              {location?.state == null && <StyledTableCell align="center">
                <Button variant='contained' sx={{ backgroundColor: '#FABC1D', color: '#5B4947', textTransform: 'none' }}
                  onClick={() => navigate(`/invoice-detail/${row.NoInvoice}`, { state: 'details' })}
                >
                  <Typography sx={{ fontWeight: 'bold' }} >Details</Typography>
                </Button>
              </StyledTableCell>}
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Tables;
