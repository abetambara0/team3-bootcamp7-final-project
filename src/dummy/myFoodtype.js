import pd1 from "../assets/asian.png"
import pd2 from "../assets/cold-drinks.png"
import pd3 from "../assets/cookies.png"
import pd4 from "../assets/dessert.png"
import pd5 from "../assets/eastern.png"
import pd6 from "../assets/hot-drinks.png"
import pd7 from "../assets/junk-food.png"
import pd8 from "../assets/western.png"

export const cardTypeFoodDatas = [
    {
        img: pd1,
        category: "Asian",
    },
    {
        img: pd2,
        category: "Cold Drink",
    },
    {
        img: pd3,
        category: "Cookies",
    },
    {
        img: pd4,
        category: "Dessert",
    },
    {
        img: pd5,
        category: "Eastern",
    },
    {
        img: pd6,
        category: "Hot Drink",
    },
    {
        img: pd7,
        category: "Junk Food",
    },
    {
        img: pd8,
        category: "Western",
    },
]
