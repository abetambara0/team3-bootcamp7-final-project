import { Box, Container, Typography } from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import BreadCrumb from "../components/BreadCrumb";
import Layout from "../components/Layout";
import Tables from "../components/Tables";

function createData(No, CourseName, Type, Schedule, Price) {
    return { No, CourseName, Type, Schedule, Price };
}

const rows = [
    createData('1', 'Tom Yum Thailand', 'Asian', 'Wednesday, 27 July 2022', 'IDR 450.000')
];

const DetailInvoice = () => {
    const navigate = useNavigate()
    const breadcrumbsDetail = [
        <Typography onClick={() => navigate('/')} sx={{ cursor: 'pointer' }}>
            Home
        </Typography>,
        <Typography onClick={() => navigate('/invoice')} sx={{ cursor: 'pointer' }} >
            Invoice
        </Typography>,
        <Typography sx={{ textDecoration: 'underline' }}>
            Details Invoice
        </Typography>
    ];
    return (
        <Layout>
            <Container sx={{ marginTop: '15px' }}>
                <BreadCrumb pages={breadcrumbsDetail} />
                <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F', marginTop: '20px' }}>
                    Details Invoice
                </Typography>
                <Box>
                    <Box>
                        <Typography sx={{ color: '#4F4F4F', marginTop: '20px' }}>
                            No. Invoice &nbsp;:&nbsp; SOU00003
                        </Typography>
                    </Box>

                    <Box sx={{ display: 'flex', width: '100%', justifyContent: 'space-between', marginTop: '10px', marginBottom: '20px' }}>
                        <Typography sx={{ color: '#4F4F4F' }}>
                            Date &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;: &nbsp;12 June 2022
                        </Typography>
                        <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F' }}>
                            Total Price : IDR 450.000
                        </Typography>
                    </Box>

                </Box>
                <Tables page='invoice' rows={rows} heads={['No', 'CourseName', 'Type', 'Schedule', 'Price']} />
            </Container>
        </Layout>
    );
};

export default DetailInvoice;
