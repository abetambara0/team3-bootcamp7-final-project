import React from "react";
import Layout from "../components/Layout";
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import BreadCrumb from "../components/BreadCrumb";
import { useNavigate } from "react-router-dom";
import { Container } from "@mui/material";
import Tables from "../components/Tables";



function createData(No, NoInvoice, Date, TotalCourse, TotalPrice) {
    return { No, NoInvoice, Date, TotalCourse, TotalPrice };
}

const rows = [
    createData('1', 'SOU00003', '12 July 2022', '1', 'IDR 450.000'),
    createData('2', 'SOU00002', '05 Februari 2022', '2', 'IDR 900.000'),
    createData('3', 'SOU00001', '30 August 2021', '1', 'IDR 600.000'),
];

const Invoice = ({ page }) => {
    const navigate = useNavigate()
    const breadcrumbsInvoice = [
        <Typography onClick={() => navigate('/')} sx={{ cursor: 'pointer' }}>
            Home
        </Typography>,
        <Typography sx={{ textDecoration: 'underline' }}>
            Invoice
        </Typography>
    ]


    return (
        <Layout>
            <Container sx={{ marginTop: '15px' }}>
                <BreadCrumb pages={breadcrumbsInvoice} />
                <Typography sx={{ fontWeight: 'bold', color: '#4F4F4F', marginTop: '20px' }}>
                    Menu Invoice
                </Typography>
                <Tables page='invoice' rows={rows} heads={['No', 'No.Invoice', 'Date', 'Total Course', 'Total Price', 'Action']} />
            </Container>
        </Layout>
    );
};

export default Invoice;
