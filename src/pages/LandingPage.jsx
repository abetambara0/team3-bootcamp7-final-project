import { Container, Typography } from "@mui/material";
import React from "react";
import LandingPageCards from "../components/Cards/LandingPageCards";
import MyClassCards from "../components/Cards/MyClassCards";
import Layout from "../components/Layout";
import { cardClassDatas } from "../dummy/myClassData.js"
import BannerTop from "../components/Banners/BannerTop";
import BannerMid from "../components/Banners/BannerMid";
import { cardTypeFoodDatas } from "../dummy/myFoodtype";
import MyFoodTypeCards from "../components/Cards/MyFoodTypeCards";
import BannerBottom from "../components/Banners/BannerBottom";


const cardDatas = [
  {
    h1: "200+",
    text: "Various cuisines available in professional class"
  },
  {
    h1: "50+",
    text: "A chef who is reliable and has his own characteristics in cooking"
  },
  {
    h1: "30+",
    text: "Cooperate with trusted and upscale restaurants"
  }
]

const LandingPage = () => {

  console.log(cardClassDatas)
  return (
    <Layout>
      <BannerTop />
      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "32px", marginBottom: "32px", alignItems: "center", gap: "10px" }}  >
        {cardDatas.map((card, index) => {
          return (
            <LandingPageCards key={index} h1={card.h1} text={card.text} />
          )
        })}
      </Container>

      <Typography variant="h4" textAlign="center" mt={5} color="#5B4947">
        More professional class
      </Typography>

      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "64px", alignItems: "center", gap: "10px", flexWrap: "wrap" }}  >
        {cardClassDatas.map((card, index) => {
          return (
            <MyClassCards key={index} img={card.img} category={card.category} name={card.name} price={card.price} />
          )
        })}
      </Container>

      <BannerMid />

      <Typography variant="h4" textAlign="center" mt={5} color="#5B4947" sx={{ marginTop: "64px" }}>
        More food type as you can choose
      </Typography>

      <Container sx={{ display: "flex", flexDirection: { xs: "column", md: "row", lg: "row" }, justifyContent: "space-around", marginTop: "64px", alignItems: "center", gap: "10px", flexWrap: "wrap" }}  >
        {cardTypeFoodDatas.map((card, index) => {
          return (
            <MyFoodTypeCards key={index} img={card.img} category={card.category} />
          )
        })}
      </Container>

    </Layout>
  );
};

export default LandingPage;
