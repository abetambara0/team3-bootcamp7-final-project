import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Button, Container, Grid, Typography } from "@mui/material";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import React, { useContext, useEffect, useState } from "react";
import Navbar from "../components/Navbar";
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import IconButton from '@mui/material/IconButton';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../Context/UserContext';
import isEmail from 'validator/lib/isEmail';


const userDatas = [
    {
        name: "admin",
        email: "admin@admin.com",
        password: "admin"
    }
]

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const LoginRegister = ({ page }) => {
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(false);
    const [alert, setAlert] = useState({ color: "", text: "" })
    const [showPassword, setShowPassword] = React.useState(false);
    const { user, login } = useContext(UserContext)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [name, setName] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const alertFunc = (color, text) => {
        setAlert({
            color: color,
            text: text
        })
        setOpen(true);
    }
    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    useEffect(() => {
        if (user.name !== "") {
            navigate('/')
        }
    }, [user]);

    const findUser = (email) => userDatas.findIndex((obj => obj.email == email))
    const handleLogin = () => {

        if (findUser(email) < 0) {
            alertFunc("error", "User Not Found")
        } else {
            const userIndex = findUser(email)
            if (userDatas[userIndex].password !== password) {
                alertFunc("error", "Please Check Your Password")
            } else {
                login({
                    name: userDatas[userIndex].name,
                    email: email,
                    auth: true
                })
                setEmail("")
                setPassword("")
                setName("")
                setConfirmPassword("")
            }
        }
    }

    const handleSignUp = () => {
        if (email === "" || name === "" || password === "" || confirmPassword === "") {
            alertFunc("warning", "Please fill all the form field")
        } else if (!isEmail(email)) {
            alertFunc("error", "Please check your email")
        } else if (password !== confirmPassword) {
            alertFunc("error", "Please provide match password with confirm password")
        } else {
            userDatas.push({
                email,
                password,
                name
            })
            setEmail("")
            setPassword("")
            setName("")
            setConfirmPassword("")
            alertFunc("success", "Please wait you are being redirected")
        }

    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <>
            <Navbar />
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={alert.color} sx={{ width: '100%' }}>
                    {alert.text}
                </Alert>
            </Snackbar>
            {
                page === "login" ?
                    <Container maxWidth="xl" sx={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh" }}>
                        <Box sx={{ width: { xs: "100%", sm: "70%" } }}>
                            <Box mb={5}>
                                <Typography variant="h4" sx={{ color: "#5B4947", fontWeight: "500" }} >
                                    Welcome Back! Cheff
                                </Typography>
                                <Typography variant="h6" sx={{ color: "#5B4947", fontWeight: "500" }}>
                                    Please login first
                                </Typography>
                            </Box>
                            <form>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="email">Email</InputLabel>
                                    <OutlinedInput
                                        id="email"
                                        type="email"
                                        label="email"
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                </FormControl>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        type={showPassword ? 'text' : 'password'}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </FormControl>
                                <Typography textAlign="start">
                                    Forgot Password? <span style={{ color: "#2F80ED", cursor: "pointer" }} onClick={() => navigate('/reset-password-email')}>Click Here</span>
                                </Typography>
                                <Box mt={1} sx={{ display: "flex", justifyContent: "flex-end" }}>
                                    <Button sx={{ backgroundColor: "#FABC1D", color: "#5B4947", width: "38px" }}
                                        onClick={handleLogin}
                                    >Login</Button>
                                </Box>
                            </form>
                            <Typography textAlign="center">
                                Don't have account? <span style={{ color: "#2F80ED", cursor: "pointer" }} onClick={() => navigate('/register')}>Sign Up here</span>
                            </Typography>
                        </Box>
                    </Container>
                    :
                    <Container maxWidth="xl" sx={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh" }}>
                        <Box sx={{ width: { xs: "100%", sm: "70%" } }}>
                            <Box mb={5}>
                                <Typography variant="h4" sx={{ color: "#5B4947", fontWeight: "500" }} >
                                    Are you ready become a professional chef?
                                </Typography>
                                <Typography variant="h6" sx={{ color: "#5B4947", fontWeight: "500" }}>
                                    Please register first
                                </Typography>
                            </Box>
                            <form>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="email">Name</InputLabel>
                                    <OutlinedInput
                                        id="name"
                                        type="text"
                                        label="name"
                                        onChange={(e) => setName(e.target.value)}
                                    />
                                </FormControl>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="email">Email</InputLabel>
                                    <OutlinedInput
                                        id="email"
                                        type="email"
                                        label="email"
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                </FormControl>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        type={showPassword ? 'text' : 'password'}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </FormControl>
                                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        type={showPassword ? 'text' : 'password'}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                        onChange={(e) => setConfirmPassword(e.target.value)}
                                    />
                                </FormControl>
                                <Box mt={1} sx={{ display: "flex", justifyContent: "flex-end" }}>
                                    <Button sx={{ backgroundColor: "#FABC1D", color: "#5B4947" }}
                                        onClick={handleSignUp}
                                    >Register</Button>
                                </Box>
                            </form>
                            <Typography textAlign="center">
                                Have account? <span style={{ color: "#2F80ED", cursor: "pointer" }} onClick={() => navigate('/login')}>Login here</span>
                            </Typography>
                        </Box>
                    </Container>
            }
        </>
    );
};

export default LoginRegister;
