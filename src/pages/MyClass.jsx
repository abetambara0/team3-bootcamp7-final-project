import React from "react";
import Layout from "../components/Layout";
import MyClassLists from "../components/MyClassLists";

const MyClass = () => {
    return (
        <Layout>
            <MyClassLists />
        </Layout>
    );
};

export default MyClass;
