import FormControl from '@mui/material/FormControl';
import { Box, Button, InputLabel, OutlinedInput, Typography } from "@mui/material";
import React from "react";
import Navbar from "../components/Navbar";
import { useNavigate } from 'react-router-dom';

const ResetPassword = ({ page }) => {
    const navigate = useNavigate()
    return (
        <Box>
            <Navbar />
            {page === "email" ?
                <Box sx={{ width: '100%', height: "100vh", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                    <Box sx={{ display: "flex", flexDirection: "column", width: "80%" }} >
                        <Typography variant="h4" >Reset Password</Typography>
                        <Typography variant="h6">Send OTP code to your email address</Typography>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="email">Email</InputLabel>
                            <OutlinedInput
                                id="email"
                                type="email"
                                label="email"
                            />
                        </FormControl>
                    </Box>
                    <Box sx={{ display: "flex", justifyContent: "end", width: "80%", gap: { xs: "1rem", sm: "1rem", md: "2rem" }, flexDirection: { xs: "column", sm: "column", md: "row" } }} >
                        <Button variant="outlined" >
                            <Typography sx={{ color: "#5B4947" }}>
                                Cancel
                            </Typography>
                        </Button>
                        <Button sx={{ backgroundColor: "#EA9E1F" }} >
                            <Typography sx={{ color: "#5B4947" }} onClick={() => navigate('/reset-password-newpass')}>
                                Confirm
                            </Typography>
                        </Button>
                    </Box>
                </Box>
                :
                <Box sx={{ width: '100%', height: "100vh", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                    <Box sx={{ display: "flex", flexDirection: "column", width: "80%" }} >
                        <Typography variant="h4" >Create Password</Typography>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="email">Password</InputLabel>
                            <OutlinedInput
                                id="password"
                                type="password"
                                label="password"
                            />
                        </FormControl>
                        <FormControl fullWidth sx={{ marginBottom: '1rem' }} variant="outlined">
                            <InputLabel htmlFor="email">Confirm Password</InputLabel>
                            <OutlinedInput
                                id="password"
                                type="password"
                                label="password"
                            />
                        </FormControl>
                    </Box>
                    <Box sx={{ display: "flex", justifyContent: "end", width: "80%", gap: { xs: "1rem", sm: "1rem", md: "2rem" }, flexDirection: { xs: "column", sm: "column", md: "row" } }} >
                        <Button sx={{ backgroundColor: "#EA9E1F" }} onClick={() => navigate('/')}>
                            <Typography sx={{ color: "#5B4947" }}>
                                Confirm
                            </Typography>
                        </Button>
                    </Box>
                </Box>
            }
        </Box >
    );
};

export default ResetPassword;
