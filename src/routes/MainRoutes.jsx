import React from "react";
import { Route, Routes } from "react-router-dom";
import DetailInvoice from "../pages/DetailInvoice";
import Invoice from "../pages/Invoice";
import LandingPage from "../pages/LandingPage";
import LoginRegister from "../pages/LoginRegister";
import MyClass from "../pages/MyClass";
import ResetPassword from "../pages/ResetPassword";
import SuccessPage from "../pages/SuccessPage";

const MainRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/login" element={<LoginRegister page="login" />} />
        <Route path="/register" element={<LoginRegister page="register" />} />
        <Route path="/reset-password-email" element={<ResetPassword page="email" />} />
        <Route path="/reset-password-newpass" element={<ResetPassword page="newpass" />} />
        <Route path="/email-confirm" element={<SuccessPage page="email" />} />
        <Route path="/myclass" element={<MyClass />} />
        <Route path="/success-purchase" element={<SuccessPage page="purchase" />} />
        <Route path="/invoice" element={<Invoice />} page="invoice" />
        <Route path="/invoice-detail/:id" element={<DetailInvoice />} />
      </Routes>
    </div>
  );
};

export default MainRoutes;
